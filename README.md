A toy font parser. You should probably use [otfm](http://opam.ocaml.org/packages/otfm/) instead.

Use:

```bash
dune exec ./demo/demo.exe ./DejaVuSans.ttf
```
