open Truetype

let show_points = false

(** Connect a signal handler, ignoring the resulting signal ID.
 * This avoids having to use [|> ignore] everywhere. *)
let (==>) (signal:(callback:_ -> GtkSignal.id)) callback =
  ignore (signal ~callback)

let scale (x, y) =
  (x, (-. y))

let _draw_rect ~cr a b =
  let (ax, ay) = scale a in
  let (bx, by) = scale b in
  Cairo.rectangle cr ax ay ~w:(bx -. ax) ~h:(by -. ay);
  Cairo.stroke cr

let line_to ~cr p =
  let (x, y) = scale p in
  Cairo.line_to cr x y

(* Mid-point of two points. *)
let (++) (x1, y1) (x2, y2) =
  (x1 +. x2) /. 2., (y1 +. y2) /. 2.

(* If we're sure the number of wasted pixels(XXX) will be less than this, then
   render the curve as a straight line. Otherwise, divide it in two and consider
   each half separately. *)
let curve_line_threshold = 10.0

let is_approx_line (x0, y0) (xm, ym) (x1, y1) =
  let a = xm -. x0 in
  let b = ym -. y0 in
  let c = x1 -. xm in
  let d = y1 -. ym in
  abs_float (a *. d -. c *. b) < curve_line_threshold

let rec draw_curve ~cr p0 p1 p2 =
  if is_approx_line p0 p1 p2 then (
    line_to ~cr p2
  ) else (
    let m1 = p0 ++ p1 in
    let m2 = p1 ++ p2 in
    let m = m1 ++ m2 in
    draw_curve ~cr p0 m1 m;
    draw_curve ~cr m m2 p2
  )

let draw_contour ~cr (p_start, path) =
  let ax, ay = scale p_start in
  Cairo.move_to cr ax ay;
  let rec outline p0 = function
    | [] -> ()
    | `Line_to p1 :: ps -> line_to ~cr p1; outline p1 ps
    | `Curve (p1, p2) :: ps -> draw_curve ~cr p0 p1 p2; outline p2 ps
  in
  outline p_start path

let on_curve_colour = (0.0, 0.8, 0.0)
let off_curve_colour = (1.0, 0.4, 0.4)
let point_radius = 4.0

let draw_contour_points ~cr (p_start, path) =
  let draw_point p (r, g, b) =
    let (x, y) = scale p in
    Cairo.set_source_rgb cr r g b;
    Cairo.arc cr x y ~r:point_radius ~a1:0.0 ~a2:(2.0 *. Float.pi);
    Cairo.fill cr
  in
  draw_point p_start on_curve_colour;
  path |> List.iter @@ function
  | `Line_to p -> draw_point p on_curve_colour
  | `Curve (ctrl, p) ->
    draw_point ctrl off_curve_colour;
    draw_point p on_curve_colour

let fill_outline ~cr layout =
  let rec aux = function
  | [] -> ()
  | { Layout.glyph; x} :: items ->
    Cairo.save cr;
    Cairo.translate cr x 0.0;
    Scaled.Glyph.contours glyph |> List.iter (draw_contour ~cr);
    Cairo.fill cr;
    Cairo.restore cr;
    aux items
  in
  aux (Layout.items layout)

let render_points ~cr layout =
  let rec aux = function
  | [] -> ()
  | { Layout.glyph; x} :: items ->
    Cairo.save cr;
    Cairo.translate cr x 0.0;
    Scaled.Glyph.contours glyph |> List.iter (draw_contour_points ~cr);
    Cairo.fill cr;
    Cairo.restore cr;
    aux items
  in
  aux (Layout.items layout)

let expose ~layout ~scale:(scale_x, scale_y) ~y (area : #GMisc.drawing_area) _ev =
  let cr = Cairo_gtk.create area#misc#window in
  Cairo.set_source_rgb cr 0.9 0.9 0.9;
  Cairo.paint cr;
  Cairo.set_source_rgb cr 0.0 0.0 0.0;
  Cairo.set_line_width cr 4.0;
  Cairo.scale cr scale_x scale_y;
  Cairo.translate cr 0.0 (0.0 -. y);
  fill_outline ~cr layout;
  if show_points then render_points ~cr layout;
  true

let show face =
  let scaled = Scaled.of_face face ~dpi:(200.0, 200.0) ~pt:72.0 in
  let msg = "Hello World! AVA" in
  let layout = Layout.of_ascii ~scaled msg in
  let scale_x = 1.0 in
  let scale_y = 1.0 in
  let width = truncate @@ Layout.logical_width layout *. scale_x in
  let height = truncate @@ (Scaled.ascent scaled -. Scaled.descent scaled) *. scale_y in
  let w = GWindow.window ~title:"ocaml-truetype" ~show:true ~width ~height ~allow_shrink:true () in
  let finished, set_finished = Lwt.wait () in
  w#connect#destroy ==> (fun () -> Lwt.wakeup set_finished ());
  let area = GMisc.drawing_area ~show:true ~packing:w#add () in
  let y = -. (Scaled.ascent scaled) in
  area#event#connect#expose ==> expose ~layout ~scale:(scale_x, scale_y) ~y area;
  finished

let main path =
  Lwt_glib.install ();
  let _ = GtkMain.Main.init () in
  Lwt_main.run begin
    let ch = open_in_bin path in
    let len = in_channel_length ch in
    let data = really_input_string ch len in
    close_in ch;
    show (Cstruct.of_string data |> Face.of_cstruct)
  end

let () =
  Printexc.record_backtrace true;
  match Sys.argv with
  | [| _; path |] -> main path
  | _ -> Fmt.failwith "Need a .ttf path"
