type t = {
  index : int;
  glyph : Raw.Glyph.t;
  metric : Raw.H_metrics.metric;
}

let points ~origin c =
  let module C = Raw.Glyph.Contour in
  let module P = Raw.Glyph.Point in
  let end_point, points' =
    C.fold c ~init:(origin, []) (fun ((x0, y0), l) p ->
        let x1 = x0 + P.delta_x p in
        let y1 = y0 + P.delta_y p in
        let ap =
          match P.on_curve p with
          | true -> `On_curve (x1, y1)
          | false -> `Off_curve (x1, y1)
        in
        ((x1, y1), ap :: l)
      )
  in
  List.rev points', end_point

(* Mid-point of two points. *)
let (++) (x1, y1) (x2, y2) =
  (x1 + x2) / 2, (y1 + y2) / 2

let to_path = function
  | [] -> assert false
  | `Off_curve _ :: _ -> assert false
  | `On_curve p_start :: ps ->
    let rec aux = function
      | `On_curve  p1 :: ps -> `Line_to p1 :: aux ps
      | `Off_curve p1 :: `On_curve p2 :: ps -> `Curve (p1, p2) :: aux ps
      | `Off_curve p1 :: (`Off_curve p2 :: _ as ps) ->
        aux (`Off_curve p1 :: `On_curve (p1 ++ p2) :: ps)
      | [] -> []
      | `Off_curve p1 :: [] -> [`Curve (p1, p_start)]
    in
    p_start, aux ps

let pp f t =
  Fmt.pf f "glyph %d (%a)" t.index Raw.H_metrics.pp_metric t.metric

let contours t =
  let cs = Raw.Glyph.contours t.glyph in
  let init = ([], (0, 0)) in
  ListLabels.fold_left cs ~init ~f:(fun (acc, origin) c ->
      let pts, end_point = points ~origin c in
      (pts :: acc), end_point
    )
  |> fst
  |> List.rev
  |> List.map to_path

let left_side_bearing t = t.metric.Raw.H_metrics.left_side_bearing
let advance t = t.metric.Raw.H_metrics.advance
