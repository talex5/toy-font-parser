type t = [ `Kern16 of Raw.Kern16.t | `Kern32 of Raw.Kern32.t ]

let kern_of_table { Raw.Kern16.coverage = _ (* TODO *); data} pair =
  match data with
  | `Format_0 data -> Raw.Kern16.Format_0.get data pair |> Raw.FWord.to_int
  | `Unknown -> 0

let kern t pair =
  match t with
  | `Kern16 t ->
    ListLabels.fold_left ~init:0 ~f:(fun acc table -> acc + kern_of_table table pair) (Raw.Kern16.tables t)
  | `Kern32 _ ->
    failwith "TODO"

let parse x = x
