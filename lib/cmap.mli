type t

val parse : Raw.Cmap.t -> t
val glyph_of : t -> Uchar.t -> int
