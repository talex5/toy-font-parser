let of_int16 x =
  if x >= 0x8000 then x - 0x10000 else x

module FWord = struct
  type t = int

  let zero = 0
  let of_int16 = of_int16
  let pp = Fmt.int
  let to_int x = x
end

module Scaler = struct
  type t =
    [ `TrueType
    | `Type1
    | `OpenType
    | `Unknown of string ]

  let pp f = function
    | `TrueType -> Fmt.string f "TrueType"
    | `Type1 -> Fmt.string f "Type1"
    | `OpenType -> Fmt.string f "OpenType"
    | `Unknown x -> Fmt.pf f "Unknown (%S)" x
end

module Platform = struct
  [%%cenum
    type platformID =
      | Unicode   [@id 0]
      | Macintosh [@id 1]
      | Microsoft [@id 3]
    [@@uint16]
  ]

  type t =
    [ `Unicode of int
    | `Macintosh of int
    | `Microsoft of int
    | `Unknown of int * int ]

  let v ~pid ~psid =
    match int_to_platformID pid with
    | Some Unicode -> `Unicode psid
    | Some Macintosh -> `Macintosh psid
    | Some Microsoft -> `Microsoft psid
    | None -> `Unknown (pid, psid)

  let pp f = function
    | `Unicode x -> Fmt.pf f "Unicode %d" x
    | `Macintosh x -> Fmt.pf f "Macintosh %d" x
    | `Microsoft x -> Fmt.pf f "Microsoft %d" x
    | `Unknown (pid, x) -> Fmt.pf f "Unknown (pid=%d;psid=%d)" pid x
end

module Format_12 = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      format : uint32;
      length : uint32;
      language : uint32;
      nGroups : uint32;
    } [@@big_endian]]

  [%%cstruct
    type group = {
      startCharCode : uint32;
      endCharCode : uint32;
      startGlyphCode : uint32;
    } [@@big_endian]]

  let fold t ~init fn =
    let groups = Cstruct.shift t sizeof_t in
    let rec aux acc g = function
      | 0 -> acc
      | i ->
        let start_char = get_group_startCharCode g |> Int32.to_int |> Uchar.of_int in
        let end_char = get_group_endCharCode g |> Int32.to_int |> Uchar.of_int in
        let start_glyph = get_group_startGlyphCode g |> Int32.to_int in
        let acc = fn acc ~start_char ~end_char ~start_glyph in
        let g = Cstruct.shift g sizeof_group in
        aux acc g (i - 1)
    in
    let n = get_t_nGroups t |> Int32.to_int in
    aux init groups n
end

module Cmap_format = struct
  type t = [
    | `Format_12 of Cstruct.t
    | `Unknown of int
  ]

  let pp f = function
    | `Format_12 _ -> Fmt.string f "Format-12"
    | `Unknown x -> Fmt.pf f "Format-%d" x
end

module Cmap = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      version : uint16;
      numberSubtables : uint16;
    } [@@big_endian]]

  [%%cstruct
    type subtable_metadata = {
      platformID : uint16;
      platformSpecificID : uint16;
      offset : uint32;
    } [@@big_endian]]

  let tables t =
    let tables = Cstruct.shift t sizeof_t in
    let n = get_t_numberSubtables t in
    List.init n @@ fun i ->
    let meta_start = i * sizeof_subtable_metadata in
    let entry = Cstruct.sub tables meta_start sizeof_subtable_metadata in
    let pid = get_subtable_metadata_platformID entry in
    let psid = get_subtable_metadata_platformSpecificID entry in
    let offset = get_subtable_metadata_offset entry |> Int32.to_int in
    let data = Cstruct.shift t offset in
    let platform = Platform.v ~pid ~psid in
    let data =
      match Cstruct.BE.get_uint16 data 0 with
      | 12 -> `Format_12 data
      | x -> `Unknown x
    in
    platform, data
end

module Head = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      version : uint32;
      fontRevision : uint32;
      checkSumAdjustment : uint32;
      magicNumber : uint32;
      flags : uint16;
      unitsPerEm : uint16;
      created : uint64;
      modified : uint64;
      xMin : uint16;
      xMax : uint16;
      yMin : uint16;
      yMax : uint16;
      macStyle : uint16;
      lowestRecPPEM : uint16;
      fontDirectionHint : uint16;
      indexToLocFormat : uint16;
      glyphDataFormat : uint16;
    } [@@big_endian]]

  let index_to_loc_format t =
    match get_t_indexToLocFormat t with
    | 0 -> `Short
    | 1 -> `Long
    | x -> Fmt.failwith "Invalid indexToLocFormat %d" x

  let units_per_em t = get_t_unitsPerEm t
end

module Glyph = struct
  type t = Cstruct.t

  [%%cstruct
    type descr = {
      numberOfContours : int16;
      xMin : int16;
      yMin : int16;
      xMax : int16;
      yMax : int16;
    } [@@big_endian]]

  let n_contours t =
    match get_descr_numberOfContours t |> of_int16 with
    | n when n >= 0 -> `Count n
    | _ -> `Compound

  module Flags : sig
    type t

    val on_curve : t
    val short_x  : t  (* The x value is one byte, not two *)
    val short_y  : t  (* The y value is one byte, not two *)
    val repeat   : t  (* The next flags byte is a repeat count *)
    val negate_x : t  (* The x value is short and should be negated *)
    val negate_y : t  (* The y value is short and should be negated *)
    val same_x   : t  (* The x value is long and the same as before *)
    val same_y   : t  (* The y value is long and the same as before *)

    val has : t -> int -> bool
  end = struct
    type t = { mask : int; value : int }

    let bit i =
      let value = 1 lsl i in
      { mask = value; value }

    let on_curve  = bit 0
    let short_x   = bit 1
    let short_y   = bit 2
    let repeat    = bit 3
    let negate_x  = { mask = short_x.value lor (1 lsl 4); value = short_x.value }
    let negate_y  = { mask = short_y.value lor (1 lsl 5); value = short_y.value }
    let same_x    = { mask = short_x.value lor (1 lsl 4); value = (1 lsl 4) }
    let same_y    = { mask = short_y.value lor (1 lsl 5); value = (1 lsl 5) }

    let has t flags = (flags land t.mask) = t.value
  end

  module Point = struct
    type t = {
      mutable flags : int;
      mutable delta_x : int;
      mutable delta_y : int;
    }

    let on_curve t = Flags.(has on_curve) t.flags
    let delta_x t = t.delta_x
    let delta_y t = t.delta_y

    let invalid = {
      flags = -1;
      delta_x = 0;
      delta_y = 0;
    }

    let pp f { flags; delta_x; delta_y } =
      Fmt.pf f "{ on_curve = %b; delta_x = %d; delta_y = %d }"
        (Flags.(has on_curve) flags) delta_x delta_y
  end

  module Flags_parser_state : sig
    type t
    (** The state of the parser. *)

    val create : Cstruct.t -> Point.t array -> t
    (** [create input points] is a new parser that reads from [input] to fill [points]. *)

    val read : t -> int
    (** [read t] reads the next uint8 from the input stream. *)

    val output : t -> n:int -> int -> unit
    (** [output t ~n v] adds [n] copies of [v] to the output. *)

    val unfinished : t -> bool
    (** [unfinished t] is true if more outputs are required. *)

    val unconsumed : t -> Cstruct.t
    (** [unconsumed t] is the remaining input.
        Raises an exception if not finished. *)
  end = struct
    type t = {
      input : Cstruct.t;
      output : Point.t array;
      mutable input_i : int;
      mutable output_i : int;
    }

    let read t =
      let i = t.input_i in
      t.input_i <- i + 1;
      Cstruct.get_uint8 t.input i

    let output t ~n flags =
      let old_i = t.output_i in
      let next_i = old_i + n in
      for i = old_i to next_i - 1 do
        t.output.(i) <- { Point.flags; delta_x = 0; delta_y = 0 }
      done;
      t.output_i <- next_i

    let unfinished t =
      t.output_i < Array.length t.output

    let create input output = {
      input;
      output;
      input_i = 0;
      output_i = 0;
    }

    let unconsumed t =
      assert (not (unfinished t));
      Cstruct.shift t.input t.input_i
  end

  let get_flags flags_data ~points =
    let open Flags_parser_state in
    let p = create flags_data points in
    while unfinished p do
      let flags = read p in
      let n = if Flags.(has repeat) flags then read p + 1 else 1 in
      output p ~n flags;
    done;
    unconsumed p

  let get_int16 data offset =
    let v = Cstruct.BE.get_uint16 data offset in
    if (v land 0x8000) = 0 then v else v - 0x10000

  (* Parse [data] as an array of offsets, as described by [flags]. *)
  let get_coords ~points data xy =
    let n_points = Array.length points in
    let short = match xy with `X -> Flags.short_x  | `Y -> Flags.short_y  in
    let neg   = match xy with `X -> Flags.negate_x | `Y -> Flags.negate_y in
    let same  = match xy with `X -> Flags.same_x   | `Y -> Flags.same_y in
    let set = match xy with
      | `X -> fun i v -> points.(i).Point.delta_x <- v
      | `Y -> fun i v -> points.(i).Point.delta_y <- v
    in
    let rec aux ~offset i =
      if i >= n_points then offset
      else (
        let f = points.(i).flags in
        let offset =
          if Flags.has short f then (
            let v = Cstruct.get_uint8 data offset in
            set i (if Flags.(has neg) f then -v else v);
            offset + 1
          ) else if Flags.has same f then (
            (* (delta is 0 by default anyway) *)
            offset
          ) else (
            set i (get_int16 data offset);
            offset + 2
          )
        in
        aux ~offset (i + 1)
      )
    in
    let offset = aux ~offset:0 0 in
    Cstruct.shift data offset

  module Contour = struct
    type t = {
      points : Point.t array;
      start : int;
      len : int;
    }

    let len t = t.len

    let get t i =
      assert (i >= 0 && i < t.len);
      t.points.(i + t.start)

    let fold t ~init f =
      let limit = t.start + t.len in
      let rec aux ~acc i =
        if i = limit then acc
        else (
          let acc = f acc t.points.(i) in
          aux ~acc (i + 1)
        )
      in
      aux ~acc:init t.start

    let pp f t =
      Fmt.pf f "[@[<v>";
      for i = t.start to t.start + t.len - 1 do
        if i > t.start then Fmt.cut f ();
        Point.pp f t.points.(i)
      done;
      Fmt.pf f "]@]"
  end

  let contours t =
    if Cstruct.len t = 0 then []
    else match n_contours t with
      | `Compound -> failwith "TODO: Compound"
      | `Count 0 -> assert false
      | `Count n_contours ->
        let simple = Cstruct.shift t sizeof_descr in
        let last_point_of_contour = Array.init n_contours (fun i ->
            let offset = i * 2 in
            Cstruct.BE.get_uint16 simple offset
          ) in
        let n_points = 1 + last_point_of_contour.(n_contours - 1) in
        let instrs = Cstruct.shift simple (n_contours * 2) in
        let instr_len = Cstruct.BE.get_uint16 instrs 0 in
        let flags_data = Cstruct.shift instrs (2 + instr_len) in
        let points = Array.make n_points Point.invalid in
        let data = get_flags flags_data ~points in
        let data = get_coords ~points data `X in
        let _data = get_coords ~points data `Y in
        List.init n_contours (fun i ->
            let start = if i = 0 then 0 else last_point_of_contour.(i - 1) + 1 in
            let len = last_point_of_contour.(i) - start + 1 in
            { Contour.points; start; len }
          )

  let x_min t = FWord.of_int16 (get_descr_xMin t)
  let y_min t = FWord.of_int16 (get_descr_yMin t)
  let x_max t = FWord.of_int16 (get_descr_xMax t)
  let y_max t = FWord.of_int16 (get_descr_yMax t)
end

module Loca = struct
  type t = {
    ty : [ `Short | `Long ];
    index : Cstruct.t;
    glyphs : Cstruct.t;
  }

  let get t i =
    let offset, next =
      match t.ty with
      | `Short ->
        let offset = i lsl 1 in
        2 * Cstruct.BE.get_uint16 t.index offset,
        2 * Cstruct.BE.get_uint16 t.index (offset + 2)
      | `Long ->
        let offset = i lsl 2 in
        Int32.to_int (Cstruct.BE.get_uint32 t.index offset),
        Int32.to_int (Cstruct.BE.get_uint32 t.index (offset + 4))
    in
    Cstruct.sub t.glyphs offset (next - offset)
end

module H_head = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      version : uint32;
      ascent : int16;
      descent : int16;
      lineGap : int16;
      advanceWidthMax : uint16;
      minLeftSideBearing : int16;
      minRightSideBearing : int16;
      xMaxExtent : int16;
      caretSlopeRise : int16;
      caretSlopeRun : int16;
      caretOffset : int16;
      _reserved : int16 [@len 4];
      metricDataFormat : int16;
      numOfLongHorMetrics : uint16;
    } [@@big_endian]]

  let n_long_hor_metrics t = get_t_numOfLongHorMetrics t
  let ascent t = get_t_ascent t |> FWord.of_int16
  let descent t = get_t_descent t |> FWord.of_int16
  let line_gap t = get_t_lineGap t |> FWord.of_int16
end

module H_metrics = struct
  type t = {
    n_long_hor_metrics : int;
    data : Cstruct.t;
  }

  type metric = {
    advance : int;
    left_side_bearing : int;
  }

  [%%cstruct
    type longHorMetric = {
      advanceWidth : uint16;
      leftSideBearing : int16;
    } [@@big_endian]]

  let long_metric t i =
    let data = Cstruct.sub t.data (sizeof_longHorMetric * i) sizeof_longHorMetric in
    let advance = get_longHorMetric_advanceWidth data in
    let left_side_bearing = get_longHorMetric_leftSideBearing data |> of_int16 in
    { advance; left_side_bearing }

  let metric t i =
    if i < t.n_long_hor_metrics then (
      long_metric t i
    ) else (
      let last_full = long_metric t (t.n_long_hor_metrics - 1) in
      let lefts = Cstruct.shift t.data (sizeof_longHorMetric * t.n_long_hor_metrics) in
      let j = i - t.n_long_hor_metrics in
      let left_side_bearing = Cstruct.BE.get_uint16 lefts (j * 2) in
      { last_full with left_side_bearing }
    )

  let pp_metric f { advance; left_side_bearing } =
    Fmt.pf f "{ advance=%d; left_side_bearing=%d }" advance left_side_bearing
end

module Kern16 = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      version : uint16;
      nTables : uint16;
    } [@@big_endian]]

  [%%cstruct
    type subtable = {
      st_version : uint16;
      length : uint16;
      coverage : uint16;
    } [@@big_endian]]

  module Coverage = struct
    type t = int

    let bit i t = t land (1 lsl i) <> 0

    let horizontal   = bit 0
    let minimum      = bit 1
    let cross_stream = bit 2
    let override     = bit 3
    let table_format t = t lsr 8
  end

  module Format_0 = struct
    type t = Cstruct.t

    [%%cstruct
      type t = {
        nPairs : uint16;
        searchRange : uint16;
        entrySelector : uint16;
        rangeShift : uint16;
      } [@@big_endian]]

    [%%cstruct
      type pair = {
        left : uint16;
        right : uint16;
        value : int16;
      } [@@big_endian]]

    let get t target =
      let data = Cstruct.shift t sizeof_t in
      let rec aux ~lo ~hi =
        if hi <= lo then FWord.zero
        else (
          let mid = (hi + lo) lsr 1 in
          let pair = Cstruct.sub data (mid * sizeof_pair) sizeof_pair in
          let item = (get_pair_left pair, get_pair_right pair) in
          let diff = compare item target in
          if diff = 0 then get_pair_value pair |> of_int16
          else if diff > 0 then aux ~lo ~hi:mid
          else aux ~lo:(mid + 1) ~hi
        )
      in
      aux ~lo:0 ~hi:(get_t_nPairs t)
  end

  type table = {
    coverage : Coverage.t;
    data : [ `Format_0 of Cstruct.t
           | `Unknown ];
  }

  let tables t =
    let rec aux data = function
      | 0 -> []
      | i ->
        let length = get_subtable_length data in
        let coverage = get_subtable_coverage data in
        let td, data = Cstruct.split data ~start:sizeof_subtable (length - sizeof_subtable) in
        let td =
          match Coverage.table_format coverage with
          | 0 -> `Format_0 td
          | _ -> `Unknown
        in
        { coverage; data = td } :: aux data (i - 1)
    in
    let data = Cstruct.shift t sizeof_t in
    let n_tables = get_t_nTables t in
    aux data n_tables
end

module Kern32 = struct
  type t = Cstruct.t

  [%%cstruct
    type t = {
      version : uint32;
      nTables : uint32;
    } [@@big_endian]]

  [%%cstruct
    type subtable = {
      length : uint32;
      coverage : uint16;
      tupleIndex : uint16;
    } [@@big_endian]]

  module Coverage = struct
    type t = int

    let vertical     t = t land 0x8000 <> 0
    let cross_stream t = t land 0x4000 <> 0
    let variation    t = t land 0x2000 <> 0
    let get_format   t = t land 0x00FF
  end

  type table = {
    coverage : Coverage.t;
    data : [ `Format_0 of Cstruct.t
           | `Format_1 of Cstruct.t
           | `Unknown ];
  }

  let tables t =
    let rec aux data = function
      | 0 -> []
      | i ->
        let length = get_subtable_length data |> Int32.to_int in
        let coverage = get_subtable_coverage data in
        let td, data = Cstruct.split data length in
        let td =
          match Coverage.get_format coverage with
          | 0 -> `Format_0 td
          | 1 -> `Format_1 td
          | _ -> `Unknown
        in
        { coverage; data = td } :: aux data (i - 1)
    in
    let data = Cstruct.shift t sizeof_t in
    let n_tables = get_t_nTables t |> Int32.to_int in
    aux data n_tables
end

module File = struct
  type t = Cstruct.t
  type tables = (string * Cstruct.t) list

  let of_cstruct t = t

  [%%cstruct
    type t = {
      scaler_type : char [@len 4];
      numTables : uint16;
      searchRange : uint16;
      entrySelector : uint16;
      rangeShift : uint16;
    } [@@big_endian]]

  [%%cstruct
    type table_metadata = {
      tag : char [@len 4];
      checkSum : uint32;
      offset : uint32;
      length : uint32;
    } [@@big_endian]
  ]

  let scaler_type t =
    match get_t_scaler_type t |> Cstruct.to_string with
    | "\x00\x01\x00\x00" | "true" -> `TrueType
    | "typ1" -> `Type1
    | "OTTO" -> `OpenType
    | x -> `Unknown x

  let tables t =
    let tables = Cstruct.shift t sizeof_t in
    let n = get_t_numTables t in
    List.init n @@ fun i ->
    let meta_start = i * sizeof_table_metadata in
    let entry = Cstruct.sub tables meta_start sizeof_table_metadata in
    let tag = get_table_metadata_tag entry |> Cstruct.to_string in
    let offset = get_table_metadata_offset entry |> Int32.to_int in
    let len = get_table_metadata_length entry |> Int32.to_int in
    let data = Cstruct.sub t offset len in
    tag, data

  let get k tables =
    try List.assoc k tables
    with Not_found -> Fmt.failwith "Table %S not found" k

  let get_opt k tables =
    List.assoc_opt k tables

  let cmap = get "cmap"
  let head = get "head"
  let hhea = get "hhea"
  let kern t =
    match get_opt "kern" t with
    | None -> None
    | Some data ->
      match Kern16.get_t_version data with
      | 0 -> Some (`Kern16 data)
      | _ -> Some (`Kern32 data)

  let hmtx t =
    let hd = hhea t in
    let n_long_hor_metrics = H_head.n_long_hor_metrics hd in
    { H_metrics.n_long_hor_metrics; data = get "hmtx" t }

  let loca t =
    let head = head t in
    let ty = Head.index_to_loc_format head in
    let index = get "loca" t in
    let glyphs = get "glyf" t in
    { Loca.ty; index; glyphs }
end
