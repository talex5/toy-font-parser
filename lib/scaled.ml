type t = {
  x_scale : float;  (* Multiply by x_scale to convert font units to pixels *)
  y_scale : float;
  face : Face.t;
}

let points_per_inch = 72.0

let of_face ~dpi:(x_dpi, y_dpi) ~pt face =
  let units_per_em = float_of_int (Face.units_per_em face) in
  let x_scale = pt *. x_dpi /. (points_per_inch *. units_per_em) in
  let y_scale = pt *. y_dpi /. (points_per_inch *. units_per_em) in
  { x_scale; y_scale; face }

module Glyph = struct
  type nonrec t = {
    scale : t;
    unscaled : Glyph.t;
  }

  let of_unscaled scale unscaled = { scale; unscaled }

  let advance t =
    float_of_int (Glyph.advance t.unscaled) *. t.scale.x_scale

  let scale_path t (start, segs) =
    let scale (x, y) = (float_of_int x *. t.scale.x_scale,
                        float_of_int y *. t.scale.y_scale) in
    let start = scale start in
    let segs = segs |> List.map @@ function
      | `Line_to p -> `Line_to (scale p)
      | `Curve (c, p) -> `Curve (scale c, scale p)
    in
    (start, segs)

  let contours t =
    Glyph.contours t.unscaled |> List.map (scale_path t)
end

let glyph t g =
  Glyph.of_unscaled t (Face.glyph t.face g)

let ascent t = float_of_int (Face.ascent t.face) *. t.y_scale
let descent t = float_of_int (Face.descent t.face) *. t.y_scale
