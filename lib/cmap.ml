module UMap = Map.Make(Uchar)

type t = (Uchar.t * int) UMap.t

let glyph_of m c =
  match UMap.find_last_opt (fun start_char -> start_char <= c) m with
  | Some (start_char, (end_char, start_glyph)) when c <= end_char ->
    start_glyph + (Uchar.to_int c - Uchar.to_int start_char)
  | _ -> 0

let unicode t =
  Raw.Cmap.tables t
  |> List.filter (function
      | `Unicode _, _ -> true
      | _ -> false)
  |> List.sort (fun x y -> compare y x)
  |> List.hd |> snd

let parse raw =
  match unicode raw with
  | `Unknown _ as t -> Fmt.failwith "Unsupported format %a" Raw.Cmap_format.pp t
  | `Format_12 t ->
    Raw.Format_12.fold t ~init:UMap.empty (fun acc ~start_char ~end_char ~start_glyph ->
        UMap.add start_char (end_char, start_glyph) acc
      )
