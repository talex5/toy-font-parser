type t = {
  file : Raw.File.t;
  head : Raw.Head.t;
  hhea : Raw.H_head.t;
  cmap : Cmap.t;
  loca : Raw.Loca.t;
  h_metrics : Raw.H_metrics.t;
  kern : Kern.t option;
}

let opt_map f = function
  | None -> None
  | Some y -> Some (f y)

let of_cstruct data =
  let file = Raw.File.of_cstruct data in
  let tables = Raw.File.tables file in
  {
    file;
    head = Raw.File.head tables;
    hhea = Raw.File.hhea tables;
    cmap = Cmap.parse (Raw.File.cmap tables);
    loca = Raw.File.loca tables;
    h_metrics = Raw.File.hmtx tables;
    kern = opt_map Kern.parse (Raw.File.kern tables);
  }

let pp f t =
  Fmt.pf f "File type : %a@." Raw.Scaler.pp (Raw.File.scaler_type t.file);
  let tables = Raw.File.tables t.file in
  Fmt.pf f "Tables: @[%a@]@." Fmt.(list ~sep:sp (pair (quote string) nop)) tables;
  let hhea = t.hhea in
  Fmt.pf f "@[<v2>Horizontal header:@,ascent=%a@,descent=%a@,lineGap=%a@]@."
    Raw.FWord.pp (Raw.H_head.ascent hhea)
    Raw.FWord.pp (Raw.H_head.descent hhea)
    Raw.FWord.pp (Raw.H_head.line_gap hhea)

let glyph t c =
  let index = Cmap.glyph_of t.cmap c in
  { Glyph.
    index;
    glyph = Raw.Loca.get t.loca index;
    metric = Raw.H_metrics.metric t.h_metrics index;
  }

let kerning t (g1, g2) =
  match t.kern with
  | None -> 0
  | Some k ->
    Kern.kern k Glyph.(g1.index, g2.index)

let ascent t = Raw.H_head.ascent t.hhea |> Raw.FWord.to_int
let descent t = Raw.H_head.descent t.hhea |> Raw.FWord.to_int
let line_gap t = Raw.H_head.line_gap t.hhea |> Raw.FWord.to_int

let units_per_em t = Raw.Head.units_per_em t.head
