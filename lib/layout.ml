type item = {
  x : float;
  glyph : Scaled.Glyph.t;
}

type t = {
  logical_width : float;
  items : item list;
}

let of_ascii ~scaled s =
  let rec aux ~acc ~x i =
    if i = String.length s then (
      { items = List.rev acc; logical_width = x }
    ) else (
      let c = Uchar.of_char s.[i] in
      let glyph = Scaled.glyph scaled c in
      let kerning =
        match acc with
        | [] -> 0.0
        | prev :: _ ->
          let prev_unscaled = prev.glyph.Scaled.Glyph.unscaled in
          let kerning_unscaled = Face.kerning scaled.Scaled.face (prev_unscaled, glyph.Scaled.Glyph.unscaled) in
          float_of_int kerning_unscaled *. scaled.Scaled.x_scale
      in
      let x = x +. kerning in
      let acc = { x; glyph } :: acc in
      let x = x +. Scaled.Glyph.advance glyph in
      aux ~acc ~x (i + 1)
    )
  in
  aux ~acc:[] ~x:0.0 0

let items t = t.items
let logical_width t = t.logical_width
