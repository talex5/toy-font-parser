module FWord : sig
  type t

  val pp : t Fmt.t
  val to_int : t -> int
end

module Glyph : sig
  type t

  val n_contours : t -> [ `Count of int | `Compound ]

  val x_min : t -> FWord.t
  val y_min : t -> FWord.t
  val x_max : t -> FWord.t
  val y_max : t -> FWord.t

  module Point : sig
    type t

    val on_curve : t -> bool
    val delta_x : t -> int
    val delta_y : t -> int

    val pp : t Fmt.t
  end

  module Contour : sig
    type t

    val len : t -> int
    val get : t -> int -> Point.t
    val fold : t -> init:'a -> ('a -> Point.t -> 'a) -> 'a

    val pp : t Fmt.t
  end

  val contours : t -> Contour.t list
end

module Loca : sig
  type t

  val get : t -> int -> Glyph.t
end

module Scaler : sig
  type t =
    [ `TrueType
    | `Type1
    | `OpenType
    | `Unknown of string ]

  val pp : t Fmt.t
end

module Platform : sig
  type t =
    [ `Unicode of int
    | `Macintosh of int
    | `Microsoft of int
    | `Unknown of int * int ]

  val pp : t Fmt.t
end

module Format_12 : sig
  type t

  val fold :
    t -> init:'a -> 
    ('a -> start_char:Uchar.t -> end_char:Uchar.t -> start_glyph:int -> 'a) ->
    'a
end

module Cmap_format : sig
  type t = [
    | `Format_12 of Format_12.t
    | `Unknown of int
  ]

  val pp : t Fmt.t
end

module Cmap : sig
  type t

  val tables : t -> (Platform.t * Cmap_format.t) list
end

module Head : sig
  type t

  val units_per_em : t -> int
end

module H_head : sig
  type t

  val ascent : t -> FWord.t
  val descent : t -> FWord.t
  val line_gap : t -> FWord.t
end

module H_metrics : sig
  type t

  type metric = {
    advance : int;
    left_side_bearing : int;
  }

  val metric : t -> int -> metric
  val pp_metric : metric Fmt.t
end

module Kern16 : sig
  type t

  module Coverage : sig
    type t

    val horizontal   : t -> bool
    val minimum      : t -> bool
    val cross_stream : t -> bool
    val override     : t -> bool
  end

  module Format_0 : sig
    type t

    val get : t -> (int * int) -> FWord.t
    (** [get t (left, right)] is the adjustment for the pair of glyphs [(left, right)]. *)
  end

  type table = {
    coverage : Coverage.t;
    data : [ `Format_0 of Format_0.t
           | `Unknown ];
  }

  val tables : t -> table list
end

module Kern32 : sig
  type t

  module Coverage : sig
    type t

    val vertical     : t -> bool
    val cross_stream : t -> bool
    val variation    : t -> bool
  end

  type table = {
    coverage : Coverage.t;
    data : [ `Format_0 of Cstruct.t
           | `Format_1 of Cstruct.t
           | `Unknown ];
  }

  val tables : t -> table list
end

module File : sig
  type t
  type tables = (string * Cstruct.t) list

  val of_cstruct : Cstruct.t -> t

  val scaler_type : t -> Scaler.t

  val tables : t -> tables

  val cmap : tables -> Cmap.t
  val head : tables -> Head.t
  val hhea : tables -> H_head.t
  val hmtx : tables -> H_metrics.t
  val loca : tables -> Loca.t
  val kern : tables -> [ `Kern16 of Kern16.t | `Kern32 of Kern32.t ] option
end
